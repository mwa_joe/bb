import { UiModule } from './../ui/ui.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AccountDetailsComponent } from './account-details/account-details.component';
import { TargetingComponent } from './targeting/targeting.component';

@NgModule({
  imports: [
    CommonModule,
    UiModule,
  ],
  declarations: [LoginComponent, UserProfileComponent, AccountDetailsComponent, TargetingComponent],
  exports: [LoginComponent]
})
export class UserMgmtModule { }
