import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntitlementsComponent } from './entitlements.component';
import { UiModule } from '../ui/ui.module';

@NgModule({
  imports: [
    CommonModule,
    UiModule,
    NgbModule
  ],
  declarations: [EntitlementsComponent]
})
export class EntitlementsModule { }
