import { BeneficiariesComponent } from './beneficiaries/beneficiaries.component';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RoutingModule } from './/routing.module';
import { UiModule } from './ui/ui.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
import { UserMgmtModule } from './user-mgmt/user-mgmt.module';
import { TransactionsComponent } from './transactions/transactions.component';
import { NotificationsModule } from './notifications/notifications.module';
import { ChequesModule } from './cheques/cheques.module';
import { ReportsComponent } from './reports/reports.component';
import { EntitlementsModule } from './entitlements/entitlements.module';
import { DashboardComponent } from './dashboard/dashboard.component';

import { MatIconRegistry, MatIconModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    TransactionsComponent,
    ReportsComponent,
    BeneficiariesComponent,
    DashboardComponent,
  ],
  imports: [ 
    HttpClientModule,
    UiModule,
    NgbModule,
    BrowserModule,
    UserMgmtModule,
    RoutingModule,
    NotificationsModule,
    ChequesModule,
    EntitlementsModule,
    MatIconModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(matIconRegistry: MatIconRegistry, domSanitizer: DomSanitizer){
    matIconRegistry.addSvgIconSet(domSanitizer.bypassSecurityTrustResourceUrl('../assets/mdi.svg'));
  }
}
