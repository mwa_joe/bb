import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BeneficiariesComponent } from '../beneficiaries/beneficiaries.component';
import { BulkComponent } from './bulk/bulk.component';
import { BillsComponent } from './bills/bills.component';
import { ScheduledOrdersComponent } from './scheduled-orders/scheduled-orders.component';
import { PaymentAuthComponent } from './payment-auth/payment-auth.component';
import { EntitlementsModule } from '../entitlements/entitlements.module';

@NgModule({
  imports: [
    CommonModule,
    EntitlementsModule
  ],
  declarations: [BeneficiariesComponent, BulkComponent, BillsComponent, ScheduledOrdersComponent, PaymentAuthComponent]
})
export class PaymentsModule { }
