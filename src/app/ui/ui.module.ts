import { RoutingModule } from './../routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout/layout.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { ContentComponent } from './content/content.component';
import { MatIcon, MatIconModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    MatIconModule
  ],
  declarations: [LayoutComponent, FooterComponent, HeaderComponent, SidenavComponent, ContentComponent],
  exports: [LayoutComponent, HeaderComponent, FooterComponent, SidenavComponent, ContentComponent]
})
export class UiModule { }