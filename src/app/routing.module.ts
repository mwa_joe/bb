import { DashboardComponent } from './dashboard/dashboard.component';
import { BeneficiariesComponent } from './beneficiaries/beneficiaries.component';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import { LoginComponent } from './user-mgmt/login/login.component';
import { EntitlementsComponent } from './entitlements/entitlements.component';

const routes: Routes = [
  { path: "login", component: LoginComponent},
  { path: "entitlements", component: EntitlementsComponent},
  { path: "beneficiaries", component: BeneficiariesComponent},
  { path: "dashboard", component: DashboardComponent},
  { path: "", redirectTo: "/dashboard", pathMatch: "full"}
]

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})

export class RoutingModule { }